﻿public var Imagen: GUITexture;
private var BotonPulsado : boolean = false;

function Update(){
    if(Input.touchCount > 0)
    {
        for (var i = 0; i < Input.touchCount; ++i) 
        {   
            if(Imagen.HitTest(Input.GetTouch(i).position))
            {        
                BotonPulsado = true;                          	           
            }
        } 
    } 
    //UNA VEZ HEMOS RECIBIDO LAS ENTRADAS. ACTUAMOS!!!
    if(BotonPulsado)
    {
		Application.LoadLevel("Tienda");
    }          		    	
    //LO PONEMOS A FALSE PARA COMPROBAR EN EL SIGUIENTE Update() SI SE SIGUEN PULSANDO O NO.
    BotonPulsado = false;	
}
